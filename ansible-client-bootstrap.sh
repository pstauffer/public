#!/bin/bash

_ANSIBLE_USER="ansible"
_ANSIBLE_USER_ID="3000"
_ANSIBLE_CLIENT_DIR="/tmp/public/ansible-client"
_AUTHORIZED_KEY=$(wget -q -O - --no-check-certificate https://bitbucket.org/barneyyy844/public/raw/master/ansible_authorized_key)

echo "# adding ansible user"
getent group ${_ANSIBLE_USER} >/dev/null || groupadd -g ${_ANSIBLE_USER_ID} ${_ANSIBLE_USER}
getent passwd ${_ANSIBLE_USER} >/dev/null || useradd -g ${_ANSIBLE_USER_ID} -u ${_ANSIBLE_USER_ID} -c "${_ANSIBLE_USER} tech user" -s /bin/bash -m -d /home/${_ANSIBLE_USER} ${_ANSIBLE_USER} 

echo "# adding temporary sudo rule"
if [[ -f /usr/bin/apt-get ]];
then
  apt-get update
  apt-get install -y sudo
else
  yum install -y sudo
fi
echo "${_ANSIBLE_USER}        ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers.d/sudo_${_ANSIBLE_USER}

echo "# copy ansible authorized_key"
[[ ! -d /home/${_ANSIBLE_USER}/.ssh ]] && mkdir /home/${_ANSIBLE_USER}/.ssh
echo ${_AUTHORIZED_KEY} > /home/${_ANSIBLE_USER}/.ssh/authorized_keys

echo "# change permissions"
chown ${_ANSIBLE_USER}.${_ANSIBLE_USER} -R /home/${_ANSIBLE_USER}/.ssh
chmod 750 /home/${_ANSIBLE_USER} /home/${_ANSIBLE_USER}/.ssh
chmod 640 /home/${_ANSIBLE_USER}/.ssh/authorized_keys


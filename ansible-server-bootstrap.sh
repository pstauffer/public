#!/bin/bash

########################
### init preperation ###
########################

if [[ ! -f /usr/local/bin/ansible ]];
then
  # update apt
  apt-get update

  # install required packages for ansible and git
  apt-get -y install python-dev python-pip git sudo

  # install ansible via pip
  pip install ansible
else
  echo "# ansible already installed"
fi

apt-get update
apt-get install -y git
cd /tmp/
git clone https://barneyyy844@bitbucket.org/barneyyy844/ansible-server.git

# run ansible playbook
cd /tmp/ansible-server
ansible-playbook init.yml

cd /tmp
rm -rf /tmp/ansible-server